extends Camera2D

var panning_speed = 300
var lerp_strength = .7
var money = 0

func _ready():
	pass

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	var panning_vector = Vector2(0,0)
	
	if Input.is_action_pressed("cam_left"):
		panning_vector.x -= 1
	if Input.is_action_pressed("cam_right"):
		panning_vector.x += 1
	if Input.is_action_pressed("cam_up"):
		panning_vector.y -= 1
	if Input.is_action_pressed("cam_down"):
		panning_vector.y += 1
	

	
	panning_vector = panning_vector.normalized()
	
	position += panning_vector * panning_speed * delta
	
	update_labels()

func update_labels():
	$HUD/MoneyLabel.text = "Money: " + str(money)
	$HUD/WaveCountdown.text = "%.2f" % $HUD/WaveCountdown/Timer.time_left
	var cat_count = get_tree().get_nodes_in_group("cats").size()
	$HUD/CatCountLabel.text = str(cat_count)
