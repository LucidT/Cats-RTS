extends Area2D

@onready var animated_sprite = $AnimatedSprite2D

var actionsQueue = []
# Called when the node enters the scene tree for the first time.
func _ready():
	add_to_group("cats")
	animated_sprite.play("Plopped")

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(_delta):
	get_inputs()
	if len(actionsQueue) != 0:
		var currentAction = actionsQueue[0]
		var dest = currentAction[0]
		var attack = currentAction[1]
		if destAccomplished(dest):
			actionsQueue.pop_front()
			currentAction = actionsQueue[0]
			dest = currentAction[0]
			attack = currentAction[1]
		#at this point I'll start handling pathing, but I gotta get back to work.

func get_inputs():
	if Input.is_action_just_pressed("left_mouse"):
		print("It is a mouse click.")
		animated_sprite.play("Walking")

func set_destination(dest, attack, queue):
	if queue:
		actionsQueue.append([dest, attack])
	else:
		actionsQueue = [[dest, attack]]

func destAccomplished(dest):
	#I figured we'd want to experiment with different metrics of "i got to my destination. Here's where we can do that.
	return position.distance_to(dest) <= 10 # because IDK!

