extends Node2D

var cat_scene = preload("res://Scenes/cat_scene.tscn")

# Called when the node enters the scene tree for the first time.
func _ready():
	pass


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	if Input.is_action_just_pressed("space_bar"):
		var cat_instance = cat_scene.instantiate()
		add_child(cat_instance)
		print("Cat Cloned")
