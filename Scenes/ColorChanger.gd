extends AnimatedSprite2D

func _ready():
	self.modulate = Color(0.851, 0.035, 0.431)

func _process(_delta):
	if Input.is_action_pressed("cam_left"):
		self.modulate = Color(1, 0 ,0)
	if Input.is_action_pressed("cam_right"):
		self.modulate = Color(0, 1 ,0)
	if Input.is_action_pressed("cam_up"):
		self.modulate = Color(0, 0 ,1)
	if Input.is_action_pressed("cam_down"):
		self.modulate = Color(1, 1 ,0)
