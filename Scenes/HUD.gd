extends Control

var selecting = false
var selection_rect = Rect2()
var selected_units = []

func _ready():
	set_process_input(true)

func _unhandled_input(event):
	if Input.is_action_just_pressed("left_mouse"):
		selecting = true
		selection_rect.position = get_global_mouse_position()
	if Input.is_action_just_pressed("right_mouse"):
			for unit in selected_units:
				unit.set_destination(event.global_position,
					Input.is_action_pressed("attack"),
					Input.is_action_pressed("Queue"))
	if Input.is_action_just_released("left_mouse"):
		selecting = false
		select_units_in_area(selection_rect)
		selection_rect = Rect2()
		queue_redraw()

func select_units_in_area(area):
	selected_units.clear()
	for unit in get_tree().get_nodes_in_group("cats"):
		if area.has_point(unit.global_position):
			selected_units.append(unit)
# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	if selecting:
		selection_rect.size = get_global_mouse_position() - selection_rect.position
		queue_redraw()

func _draw():
	draw_rect(selection_rect, "red",false,1)
